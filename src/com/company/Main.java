package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here
        //exercise 1
        System.out.println(exercise1(2.4523f,-3.8573f, 56.1234f, 3.231f));
        System.out.println();
        //exercise 2
        System.out.println(exercise2(1,3));
        //exercise 3
        exercise3(-5);
        //exercise4
        System.out.println(exercise4(4));
        //exercise5
        exercise5("Елена");
        //local changes to enable commit
        String[] sm = {"A", "B", "C", "D"};
        for (String o : sm){
            System.out.println(o + "");
        }
    }
    public static float exercise1(float a, float b, float c, float d){
        float result;
        result = a * (b + (c / d));
        return result;
    }
    public static boolean exercise2(int a,int b){
        int sum = a +b;
        return sum >= 10 && sum <= 20;
    }
    public static void exercise3(int a){
        if(a >= 0){
            System.out.println("Передали положительное число");
        }else {
            System.out.println("Передали отрицательное число");
        }
    }
    public static boolean exercise4(int a){
        return a < 0;
    }
    public static void exercise5(String str){
        System.out.println("Привет, "+ str);
    }
}
