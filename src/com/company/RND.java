package com.company;

import java.util.Random;
import java.util.Scanner;

public class RND {
    public  static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args){
        Random random = new Random();
        int x = random.nextInt(3)-1;
        int min = 2;
        int range =8;
        int y = (int)(Math.random()*range) + min;//Выдает double или float
        System.out.println(x);
        System.out.println(y);
        //Запомнить про строки
        String s1 = "A";
        String s2 = "A";
        String s3 = "B";
        String str = "Evgenia";
        System.out.println(s1.equals(s2));
        System.out.println(s1.equals(s3));
        char ch = str.charAt(0);
        System.out.println(str.length());
        System.out.println(ch);
        HW(s1);
    }
    public static int getNumberScanner(String message, int min, int max){
        int x;
        do{
            System.out.println(message);
            x = scanner.nextInt();
        }while (x<min || x > max);
        return x;
    }
    public static void HW(String args) {
        int goal = 7;
        Random random = new Random();
        int x = random.nextInt(10);
        goal = x;
        System.out.println("goal = " + goal );
        Scanner scanner = new Scanner(System.in);
        for ( int i =0 ; i < 3; i++){
            int d = getNumberScanner("Введите значение от 0 до 9",0,9);
            if ( d == goal) {
                System.out.println("Вы победили, верное значение = " + d);
                int k = getNumberScanner("Повторить игру еще раз? 1 – да / 0 – нет",0,1);
                if (k == 0) break;
                if (k == 1) {
                    i = -1;
                    int z = random.nextInt(10);
                    goal = z;
                    System.out.println("goal = " + goal );
                }
            }
            if (d > goal ) System.out.println(" Загаданное значение меньше указанного");
            if (d < goal ) System.out.println(" Загаданное значение больше указанного");
            if (i == 2) {
                System.out.println("Поражение, попытки закончились, было загадано число " + goal);
                int p = getNumberScanner("Повторить игру еще раз? 1 – да / 0 – нет",0,1);
                if (p == 0) break;
                if (p == 1) {
                    i = -1;
                    int z = random.nextInt(10);
                    goal = z;
                    System.out.println("goal = " + goal );
                }
            }
        }
    }
}
