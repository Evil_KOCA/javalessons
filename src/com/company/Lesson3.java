//Сканер
package com.company;

import java.util.Scanner;

public class Lesson3 {
    public  static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args){
        int d = getNumberScanner("Введите значение от 5 до 10",5,10);
        System.out.println("d =  " + d);
        String str = scanner.next();
        System.out.println(str);
        String str2 = scanner.nextLine();//Next и Nextline отличаются переводом строки на след. абзац
    }
    public static int getNumberScanner(String message, int min, int max){
        int x;
        do{
            System.out.println(message);
            x = scanner.nextInt();
        }while (x<min || x > max);
        return x;
    }
}
