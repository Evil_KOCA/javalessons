package com.company;

import java.util.Random;

public class DZLesson32 {
    public static void main(String[] args) {
        int[][] LeftMatrix= new int[2][5];
        LeftMatrix = RandMatrix(LeftMatrix);
        PrintMatrix(LeftMatrix);
        int[][] RightMatrix= new int[5][3];
        RightMatrix = RandMatrix(RightMatrix);
        PrintMatrix(RightMatrix);
        int[][] MultiMatrix = MultiMatrix(LeftMatrix,RightMatrix);
        PrintMatrix(MultiMatrix);
        int[][] SpiralMatrix1 = SpiralMatrix1(10);
        PrintMatrix(SpiralMatrix1);
    }
    public static int[][] RandMatrix ( int[][] arr){
        Random random = new Random();
        for ( int i = 0; arr.length > i; i++){
            for (int j = 0; arr[0].length > j; j++){
                arr[i][j] = random.nextInt(10);
            }
        }
        return arr;
    }
    public static void PrintMatrix(int[][] arr){
        for ( int i = 0; arr.length > i; i++){
            for (int j = 0; arr[0].length > j; j++){
                System.out.printf("%4d", arr[i][j]);
            }
            System.out.println(" ");
        }
        System.out.println(" ");
    }
    public static  int[][] MultiMatrix(int [][] LeftMatrix, int[][] RightMatrix){
        int[][] ResultMatrix = new int[LeftMatrix.length][RightMatrix[0].length];
        for(int i = 0; ResultMatrix.length > i; i++){
            for (int j = 0; ResultMatrix.length > j; j ++){
                for(int n = 0; n < ResultMatrix[0].length; n++){
                    ResultMatrix[i][j] = ResultMatrix[i][j] + LeftMatrix[i][n]*RightMatrix[n][j];
                }
            }
        }
        return ResultMatrix;
    }
    public static int[][] SpiralMatrix1(int arr){
        int k = arr*2+1;
        int[][] SpiralMatrix = new int[k][k];
        int s = 1; //начинаем заполнять с единицы
        for ( int n = 0; s < k*k & n<k; n++){
            for (int i = 0; i < SpiralMatrix.length-2*n; i ++) { //Идем направо
                SpiralMatrix[n][n+i] = s;
                s++;
            }
            for ( int i = 0 ; i < SpiralMatrix.length-2-2*n; i ++){//Идем вниз
                SpiralMatrix[i+1+n][SpiralMatrix.length-1-n] = s;
                s++;
            }
            for ( int i = 0 ; i < SpiralMatrix.length-2*n; i++){ //Идем влево
                SpiralMatrix[SpiralMatrix.length-1-n][SpiralMatrix.length-1-i-n] = s;
                s++;
            }
            for (int i = 0; i < SpiralMatrix.length - 2-2*n; i++) { //Up
                SpiralMatrix[SpiralMatrix.length-2-i-n][n] = s;
                s++;
            }
        }
        SpiralMatrix[arr][arr] = s;
        return SpiralMatrix;
    }
}
